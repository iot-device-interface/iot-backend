package de.dennis.iotserver.controller;

import de.dennis.iotserver.entity.UserEntity;
import de.dennis.iotserver.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/user")
public class UserController {

    private static Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/create")
    public ResponseEntity<Object> createUser(@RequestBody UserEntity userEntity) {
        String username = userEntity.getUsername();
        String email = userEntity.getEmail();

        if (this.userRepository.findByEmail(email) == null && this.userRepository.findByUsername(username) == null) {
            LOG.info("Created a new user! " + username);
            this.userRepository.save(userEntity);
            return ResponseEntity.status(HttpStatus.OK).build();
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    /*
    @PostMapping(path = "api/v1/user/login")
    public ResponseEntity<Object> loginUser(@RequestBody LoginForm loginForm) {
        if (this.userRepository.findByUsername(loginForm.getUsername()) != null) {
            UserEntity userEntity = this.userRepository.findByUsername(loginForm.getUsername());

            if (userEntity.getPassword().equals(loginForm.getPassword()))
                return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
    */

    @GetMapping(path = "/{username}")
    public ResponseEntity<UserEntity> getUserByUsername(@PathVariable("username") String username) {
        if (this.userRepository.findByUsername(username) != null)
            return ResponseEntity.ok(this.userRepository.findByUsername(username));
        else
            return ResponseEntity.notFound().build();
    }

    @PutMapping(path = "/{username}")
    public UserEntity updateUser(@PathVariable("username") String username, @RequestBody UserEntity userEntity) {
        if (this.userRepository.findByUsername(username) != null) {
            this.userRepository.save(userEntity);
            LOG.debug("User " + username + " updated!");
            return userEntity;
        }else {
            return null;
        }
    }

    @GetMapping(path = "/list", produces = "application/json")
    public List<UserEntity> listEntity() {
        List<UserEntity> registries = new ArrayList<>();
        userRepository.findAll().forEach(registries::add);
        return registries;
    }

    @DeleteMapping(path = "/{username}")
    public ResponseEntity<Object> deleteUser(@PathVariable("username") String username) {
        if (this.userRepository.findByUsername(username) != null) {
            this.userRepository.delete(this.userRepository.findByUsername(username));
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }
    }

}
