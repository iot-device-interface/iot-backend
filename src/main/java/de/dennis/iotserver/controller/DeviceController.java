package de.dennis.iotserver.controller;

import de.dennis.iotserver.entity.DeviceEntity;
import de.dennis.iotserver.entity.RegistryEntity;
import de.dennis.iotserver.repository.DeviceRepository;
import de.dennis.iotserver.repository.RegistryRepository;
import de.dennis.iotserver.storage.TemporaryStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/devices")
public class DeviceController {

    private static Logger LOG = LoggerFactory.getLogger(DeviceController.class);

    @Autowired
    private RegistryRepository registryRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @PostMapping(path = "/register")
    public ResponseEntity<Object> createDevice(@RequestBody DeviceEntity deviceEntity) {
        String authKey = deviceEntity.getAuthenticationKey();
        String name = deviceEntity.getName();

        LOG.debug("Request in DeviceController#createDevice");

        if (deviceRepository.findByName(name) == null) {
            if (checkKey(authKey, name)) {
                LOG.info("Registered a new device! {} @ {}", name, deviceEntity.getIpv4());
                deviceRepository.save(deviceEntity);

                return ResponseEntity.ok().build();
            }else {
                LOG.debug("Bad Request in DeviceController#createDevice");
                return ResponseEntity.badRequest().build();
            }

        }else
            LOG.debug("Not found in DeviceController#createDevice");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    /*
    @GetMapping(path = "api/v1/devices/get/{id}")
    public ResponseEntity<Object> getDeviceById(@PathVariable("id") String id) {
        if (deviceRepository.findById(Long.valueOf(id)).isPresent()) {
            Optional<DeviceEntity> devices = deviceRepository.findById(Long.valueOf(id));

            return ResponseEntity.ok(devices.get());
        }else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

     */

    @GetMapping(path = "/{name}")
    public DeviceEntity getDeviceByName(@PathVariable("name") String name) {
        if (deviceRepository.findByName(name) != null) {
            LOG.debug("Returned a device in DeviceController#getDeviceByName");
            return deviceRepository.findByName(name);
        }
        throw new IllegalArgumentException(String.format("No device found for '%s'", name));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/list", produces = "application/json")
    public List<DeviceEntity> listDevices() {
        LOG.debug("Returning all listed devices");
        List<DeviceEntity> devices = new ArrayList<>();
        deviceRepository.findAll().forEach(devices::add);
        return devices;
    }

    @GetMapping(path = "/login")
    public ResponseEntity<Object> loginDevice(@RequestHeader("authKey") String key, @RequestParam String deviceName) {
        if (deviceName != null) {
            if (this.checkKey(key, deviceName)) {
                DeviceEntity device = deviceRepository.findByName(deviceName);
                TemporaryStorage.getOnlineDevices().put(deviceName, device);

                LOG.debug("Device " + deviceName + " logged in!");

                return ResponseEntity.ok().build();
            }else {
                LOG.debug("Device was not found. Name: " + deviceName);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }else
            return ResponseEntity.badRequest().build();
    }

    @GetMapping(path = "/logout")
    public ResponseEntity<Object> logoutDevice(@RequestHeader("authKey") String key, @RequestParam String deviceName) {
        if (deviceName != null) {
            if (this.checkKey(key, deviceName)) {
                if (TemporaryStorage.getOnlineDevices().containsKey(deviceName)) {
                    TemporaryStorage.getOnlineDevices().remove(deviceName);
                    LOG.debug("Device " + deviceName + " logged out!");
                    return ResponseEntity.ok().build();
                }else {
                    return ResponseEntity.badRequest().build();
                }
            }else {
                return ResponseEntity.notFound().build();
            }
        }else
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    public boolean checkKey(String key, String name) {
        if (this.registryRepository.findByName(name) != null) {
            RegistryEntity registryEntity = this.registryRepository.findByName(name);

            if (registryEntity.getName().equals(name) && registryEntity.getAuthenticationKey().equals(key)) return true;

            return false;
        }

        return false;
    }


}
