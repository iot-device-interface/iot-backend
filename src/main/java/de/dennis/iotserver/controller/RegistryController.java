package de.dennis.iotserver.controller;

import de.dennis.iotserver.entity.RegistryEntity;
import de.dennis.iotserver.repository.RegistryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/registry")
public class RegistryController {

    private static Logger LOG = LoggerFactory.getLogger(RegistryController.class);

    @Autowired
    private RegistryRepository registryRepository;

    @PostMapping(path = "/add")
    public ResponseEntity<Object> addRegistry(@RequestBody RegistryEntity registryEntity) {
        String registryName = registryEntity.getName();

        if (this.registryRepository.findByName(registryName) == null) {
            this.registryRepository.save(registryEntity);
            LOG.info("A new registry created!");
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping(path = "/remove")
    public ResponseEntity<Object> removeRegistry(@RequestParam("name") String registryRemove) {
        if (this.registryRepository.findByName(registryRemove) != null) {
            LOG.info("Registry " + registryRemove + " deleted.");
            this.registryRepository.delete(this.registryRepository.findByName(registryRemove));
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(path = "/get/{name}")
    public ResponseEntity<RegistryEntity> getRegistry(@PathVariable("name") String name) {
        if (this.registryRepository.findByName(name) != null) {
            return ResponseEntity.ok(this.registryRepository.findByName(name));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "list", produces = "application/json")
    public List<RegistryEntity> listEntity() {
        List<RegistryEntity> registries = new ArrayList<>();
        registryRepository.findAll().forEach(registries::add);
        return registries;
    }

}
