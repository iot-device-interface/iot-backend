package de.dennis.iotserver.storage;

import com.google.common.collect.Maps;
import de.dennis.iotserver.entity.DeviceEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;

@Getter @Setter
public class TemporaryStorage {

    @Getter
    public static HashMap<String, DeviceEntity> onlineDevices = Maps.newHashMap();
}
