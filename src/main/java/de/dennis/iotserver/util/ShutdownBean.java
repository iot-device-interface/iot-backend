package de.dennis.iotserver.util;

import de.dennis.iotserver.IoTServer;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

@Component
public class ShutdownBean {

    @PreDestroy
    public void destroy() {
        /*
          * Shutdown
         */
    }

}
