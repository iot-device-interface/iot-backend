package de.dennis.iotserver.util;

public class RouteBuilder {

    public DefaultRoutes route;

    public RouteBuilder(DefaultRoutes route) {
        this.route = route;
    }

    public String build() {
        return "/api/" + DefaultRoutes.API_VERSION.getRoute() + "/" + this.route.getRoute();
    }

}
