package de.dennis.iotserver.util.restbody;

import lombok.Data;

@Data
public class LoginForm {

    private String username;
    private String password;

}
