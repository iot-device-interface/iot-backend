package de.dennis.iotserver.util.restbody;

import lombok.Data;

@Data
public class RegistryRemove {

    private String name;
}
