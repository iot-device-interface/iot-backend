package de.dennis.iotserver.util;

import lombok.Getter;

public enum DefaultRoutes {

    /*
     * API Version for routing
     */
    API_VERSION("v1"),

    /*
     * Routes
     */

    DEVICE_CONTROLLER_DEFAULT("device");

    public String route;

    DefaultRoutes(String route) {
        this.route = route;
    }

    public String getRoute() {
        return route;
    }
}
