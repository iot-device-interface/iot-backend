package de.dennis.iotserver.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

@Entity
@Table
@ToString

@Getter
@Setter
public class UserEntity {

    @Id
    @GeneratedValue
    private long id;

    @NotBlank(message = "The username can not be null!")
    private String username;

    @NotBlank(message =  "The password can not be null")
    private String password;

    @Email
    @NotBlank(message = "The email can not be null!")
    private String email;

    private Timestamp lastLogin;

    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());

}
