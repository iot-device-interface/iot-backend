package de.dennis.iotserver.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table
@ToString

@Getter
@Setter
public class RegistryEntity {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "The name can not be null")
    private String name;

    private String authenticationKey;

    private long created;

    public RegistryEntity() {
        this.created = System.currentTimeMillis();
        this.authenticationKey = UUID.randomUUID().toString().replace("-", "");
    }
}
