package de.dennis.iotserver.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
@ToString

@Getter
@Setter
public class SessionEntity {

    @Id
    @GeneratedValue
    private long id;

    private String username;

}
