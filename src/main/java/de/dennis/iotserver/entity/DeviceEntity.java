package de.dennis.iotserver.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Sort;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table
@ToString

@Getter
@Setter
public class DeviceEntity {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "Hostname can not be null!")
    private String hostname;

    @NotNull(message = "IpV4 can not be null!")
    private String ipv4;

    @NotNull(message = "Name of device can not be null")
    private String name;

    @NotNull(message = "Authentication key can not be null")
    private String authenticationKey;

    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());

    private Timestamp lastSeen;

    private boolean online = false;

    private String description;

}
