package de.dennis.iotserver.repository;

import de.dennis.iotserver.entity.DeviceEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DeviceRepository extends CrudRepository<DeviceEntity, Long> {

    DeviceEntity findByName(String name);

}
