package de.dennis.iotserver.repository;

import de.dennis.iotserver.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    UserEntity findByUsername(String name);
    UserEntity findByEmail(String email);

}
