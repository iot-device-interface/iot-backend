package de.dennis.iotserver.repository;

import de.dennis.iotserver.entity.RegistryEntity;
import org.springframework.data.repository.CrudRepository;

public interface RegistryRepository extends CrudRepository<RegistryEntity, Long> {

    RegistryEntity findByName(String name);

}
