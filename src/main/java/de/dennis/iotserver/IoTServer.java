package de.dennis.iotserver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;

@Getter
public class IoTServer {

    @Getter
    public static Gson gson;

    public void setup() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    /*

    =======

    29.09.2020:
    - Finished DeviceController + /login & /logout
    - Added CustomResponse
    - RegistrySystem -> Erstellen eines Gerätes inkl. Key um das Gerät zu registrieren
    - Library für die Microcontroller angefangen
    - Angular angeschaut -> eventuelle Alternative: Einfache PHP-Seiten
     */

}
