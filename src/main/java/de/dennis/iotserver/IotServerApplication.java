package de.dennis.iotserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class IotServerApplication extends SpringBootServletInitializer {

	private static IoTServer ioTServer = new IoTServer();

	public static void main(String[] args) {
		SpringApplication.run(IotServerApplication.class, args);
		ioTServer.setup();
	}

}
